package com.example.lou.hurimoney;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;




public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager fragmentManager = getFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragmentManager.beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_accueil_layout) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();

        } else if (id == R.id.nav_first_layout) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new FirstFragment()).commit();
	
	    } else if (id == R.id.nav_second_layout) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SecondFragment()).commit();

	    } else if (id == R.id.nav_third_layout) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ThirdFragment()).commit();

        } else if (id == R.id.nav_four_layout) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new FourFragment()).commit();

        } else if (id == R.id.nav_six_layout) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SixFragment()).commit();

        } else if (id == R.id.nav_contact) {
            //fragmentManager.beginTransaction().replace(R.id.content_frame, new HeightFragment()).commit();
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+2211441));
            startActivity(callIntent);

        } else if (id == R.id.nav_nine_layout) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new NineFragment()).commit();
    }

       

	DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//RECHARGEMENT DE CRÉDIT

    public void showDialog(View view) {
        FragmentManager manager = getFragmentManager();
        Rechargem rechargem = new Rechargem();
        rechargem.show(manager, "Rechargem");
    }


    public void showDialogo(View view) {
        FragmentManager manager = getFragmentManager();
        Recharge recharge = new Recharge();
        recharge.show(manager, "Recharge");
    }

    //PAIEMENT FACTURE

    public void showDialogeau(View view) {
        FragmentManager manager = getFragmentManager();
        Facture_eau facture_eau = new Facture_eau();
        facture_eau.show(manager, "Facture_eau");
    }

    public void showDialogelec(View view) {
        FragmentManager manager = getFragmentManager();
        Facture_elec facture_elec = new Facture_elec();
        facture_elec.show(manager, "Facture_elec");

    }

    public void showDialogtel(View view) {
        FragmentManager manager = getFragmentManager();
        Facture_tel facture_tel = new Facture_tel();
        facture_tel.show(manager, "Facture_tel");
    }

    public void showDialogcanal(View view) {
        FragmentManager manager = getFragmentManager();
        Facture_canal facture_canal = new Facture_canal();
        facture_canal.show(manager, "Facture_canal");
    }

//Mon compte

    public void showDialogcod (View view) {
        FragmentManager manager = getFragmentManager();
        Code_secret code_secret = new Code_secret();
        code_secret.show(manager, "Code_secret");
    }

    public void showDialogsol (View view) {
        FragmentManager manager = getFragmentManager();
        Consult_solde consult_solde = new Consult_solde();
        consult_solde.show(manager, "Consult_solde");
    }

//Nos agences

    public void showDialoganj(View view){
        FragmentManager manager = getFragmentManager();
        Anjouan Anjouan = new Anjouan();
        Anjouan.show(manager, "Anjouan");
    }

    public void showDialogGC(View view){
        FragmentManager manager = getFragmentManager();
        Grande_Comore Grande_Comore = new Grande_Comore();
        Grande_Comore.show(manager, "Grande_Comore");
    }

    public void showDialogmoh(View view){
        FragmentManager manager = getFragmentManager();
        Moheli Moheli = new Moheli();
        Moheli.show(manager, "Moheli");
    }

// Accueil

    public void showFragment1(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new FirstFragment()).commit();
    }

    public void showDialogtransfer(View view){
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new SecondFragment()).commit();
    }

    public void showFragment3(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new ThirdFragment()).commit();
    }


    public void showFragment5(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new FourFragment()).commit();
    }

    public void showFragment6(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new FiveFragment()).commit();
    }

    public void showFragment7(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new SixFragment()).commit();
    }

//Service client

    public void showFragment9(View view) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+2211441));
        startActivity(callIntent);
    }

//

    public void showFragment10(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new NineFragment()).commit();
    }

    public void showFragment11(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new TenFragment()).commit();
    }
}



