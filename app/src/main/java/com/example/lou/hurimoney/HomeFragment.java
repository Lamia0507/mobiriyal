package com.example.lou.hurimoney;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class    HomeFragment extends Fragment
{

    private static final String PREF_USER_NUMERO = "numeroUtilisateur";
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        String numero =  sharedPreferences.getString(PREF_USER_NUMERO,"");
        Toast.makeText(getActivity(), "numero :"+numero, Toast.LENGTH_SHORT).show();
         return view;



    }



}



