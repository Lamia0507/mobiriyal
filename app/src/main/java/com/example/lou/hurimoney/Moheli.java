package com.example.lou.hurimoney;

import android.app.DialogFragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;







public class Moheli extends DialogFragment implements AdapterView.OnItemClickListener {

    ListView mylist2;
    String[] LIEUX2= new String[] {"Fomboni", "Mdjoiyézi", "Nioumachioi", "Ouala", "Miringoni"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.activity_moheli, null, false);
        mylist2 = (ListView) view.findViewById(R.id.listView);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.moheli_list, LIEUX2);
        mylist2.setAdapter(adapter);
        mylist2.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        dismiss();

        Toast.makeText(getActivity(),LIEUX2[position],Toast.LENGTH_SHORT).show();
    }
}

