package com.example.lou.hurimoney;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class Rechargem extends DialogFragment
{
    Button validateButton;
    EditText montant,codeSecret;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_rechargem, container,false);
        validateButton = (Button)view.findViewById(R.id.rechargement_btn);

        montant = (EditText)view.findViewById(R.id.rechargement_montant);

        codeSecret = (EditText)view.findViewById(R.id.rechargement_secret);



        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String txtMontant = montant.getText().toString();
                String txtCode = codeSecret.getText().toString();
                RechargementBackground rechargementBackground = new RechargementBackground();
                rechargementBackground.execute(txtMontant,txtCode,"770881994");


            }
        });
        return view;
    }



    private class RechargementBackground extends AsyncTask<String,Void,String>
    {
        ProgressDialog progressBar;
        @Override
        protected void onPreExecute()
        {
            progressBar = new ProgressDialog(getContext());
            progressBar.setMessage("Veuillez Patientez");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String montant = strings[0];
            String code = strings[1];
            String numero = strings[2];
            String login_url = "http://192.168.43.248/recharge.php";
            try {

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("montant", "UTF-8")+ "=" + URLEncoder.encode(montant, "UTF-8")+"&"
                        + URLEncoder.encode("code", "UTF-8")+ "=" + URLEncoder.encode(code, "UTF-8")+"&"
                        + URLEncoder.encode("phone", "UTF-8")+ "=" + URLEncoder.encode(numero, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String result="";
                String line;
                while ((line=bufferedReader.readLine())!=null){
                    result +=line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            progressBar.dismiss();
            if (s.equals(" non"))
            {
                Toast.makeText(getContext(), "Montant non disponible, veuillez revoir le solde de votre compte", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getContext(), "Achat effectué avec succes, Merci d'avoir choisi Comores Telecom", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        }
    }
}
