package com.example.lou.hurimoney;

/**
 * Created by lou on 20/07/16.
 */
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


public class FourFragment extends Fragment{

    EditText codeSecret;
    Button validation;
    TextView solde;
    private static final String PREF_USER_NUMERO = "numeroUtilisateur";
    String numero;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View myView = inflater.inflate(R.layout.four_layout, container, false);

        codeSecret = (EditText)myView.findViewById(R.id.editText2);
        validation = (Button)myView.findViewById(R.id.consultation_solde_btn);
        solde = (TextView)myView.findViewById(R.id.solde);

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        numero =  sharedPreferences.getString(PREF_USER_NUMERO,"");

        validation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if (!codeSecret.getText().toString().isEmpty())
                {
                ConsultaionSoldeBackground soldeBackground = new ConsultaionSoldeBackground();
                soldeBackground.execute(numero,codeSecret.getText().toString());
                }
                else
                {
                    Toast.makeText(getActivity(), "Veuillez entrer le code", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return myView;
    }

    private class ConsultaionSoldeBackground extends AsyncTask<String,Void,String>
    {

        ProgressDialog alertDialog;

        @Override
        protected void onPreExecute() {
            alertDialog = new ProgressDialog(getContext());
            alertDialog.setMessage("Veuillez patientez");
            alertDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            alertDialog.show();
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String soldeUrl = "http://192.168.43.248/solde.php";
            try {

                String password = strings[1];
                String numero = strings[0];
                URL url = new URL(soldeUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("numero", "UTF-8")+ "=" + URLEncoder.encode(numero, "UTF-8")+"&"
                        + URLEncoder.encode("password", "UTF-8")+ "=" + URLEncoder.encode(password, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String result="";
                String line;
                while ((line=bufferedReader.readLine())!=null){
                    result +=line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            if (s != null)
            {
                solde.setText("Le solde de votre compte est de : "+s+" Fcfa");

            }
            else Toast.makeText(getContext(), "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show();
        }
    }
}

